import httpReq from "./utils/httpReq";
import useFetch from "./useFetch";


export const useFetchUrl = (url) => {
  const fetcher = (
    url === null || url === undefined || url === ""
  ) ? null : (fetchOptions) => {
    return httpReq.get(url, fetchOptions);
  }

  return useFetch(fetcher, [url]);  //fetchState
};

export default useFetchUrl;
