import getCookie from './getCookie';
import Timeout from './timeout';

export const getHttpReq = fetcher => {
  return {
    delete: (url, data, fetchOptions) => {
      let options = {
        method: 'DELETE',
        credentials: 'same-origin',
        headers: {
          'X-CSRFToken': getCookie('csrftoken'),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }

      if (fetchOptions){
        if (fetchOptions.signal !== undefined){
          options.signal = fetchOptions.signal;
        }
        if (fetchOptions.timeout !== undefined){
          options.timeout = fetchOptions.timeout
        }
      }
      return fetcher(url, options);
    },
    get: (url, fetchOptions) => {

      let options = {
        method: 'GET',
        credentials: 'same-origin',
        headers: {
          Accept: 'application/json'
        }
      }
      if (fetchOptions){
        if (fetchOptions.signal !== undefined){
          options.signal = fetchOptions.signal;
        }
        if (fetchOptions.timeout !== undefined){
          options.timeout = fetchOptions.timeout
        }
      }

      return fetcher(url, options);
    },
    put: (url, data, fetchOptions) => {
      let options = {
        method: 'PUT',
        credentials: 'same-origin',
        headers: {
          'X-CSRFToken': getCookie('csrftoken'),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
      if (fetchOptions){
        if (fetchOptions.signal !== undefined){
          options.signal = fetchOptions.signal;
        }
        if (fetchOptions.timeout !== undefined){
          options.timeout = fetchOptions.timeout
        }
      }

      return fetcher(url, options);
    },
    post: (url, data, fetchOptions) => {
      let options = {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
          'X-CSRFToken': getCookie('csrftoken'),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }
      if (fetchOptions){
        if (fetchOptions.signal !== undefined){
          options.signal = fetchOptions.signal;
        }
        if (fetchOptions.timeout !== undefined){
          options.timeout = fetchOptions.timeout
        }
      }

      return fetcher(url, options);
    },
    patch: (url, data, fetchOptions) => {
      let options = {
        method: 'PATCH',
        credentials: 'same-origin',
        headers: {
          'X-CSRFToken': getCookie('csrftoken'),
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      }

      if (fetchOptions){
        if (fetchOptions.signal !== undefined){
          options.signal = fetchOptions.signal;
        }
        if (fetchOptions.timeout !== undefined){
          options.timeout = fetchOptions.timeout
        }
      }

      return fetcher(url, options);
    }
  };
};


async function customFetch(url, fetchOptions)
{

  let response;
  let dataJSON;

  try {
    // response = await fetch(...args);

    const debugStart = new Date()
    const timeout = new Timeout({ milliseconds: fetchOptions.timeout || 30000 })

    response = await Promise.race([
      fetch(url, fetchOptions), timeout.start
    ]).finally(_ => { timeout.clear() });

  } catch (error){
    // if (error.message === 'Timeout' || error.message === 'Network request failed') {
    //   // retry possible?
    // }

    throw {
      "type": "network",
      "error": error
    };
  }

  const httpError = !response.ok;
  // response.ok => in range: 200-299  (400-600)

  try {
    dataJSON = await response.json();
  } catch (e){
    if (response.status === 204){
      return {
        "status": response.status,
        "response": response,
        "json": dataJSON
      };
    }
    throw {
      "type": "parse",
      "status": response.status,
      "response": response
    };
  }

  if (httpError){
    // we had dataJSON!
    throw {
      "type": "server",
      "status": response.status,
      "response": response,
      "json": dataJSON,
    };
  }
  return dataJSON;
}



export default getHttpReq(customFetch);
