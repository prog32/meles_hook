import _each from "lodash/each";

export const asUrlQuery = kwargs => {
  let urlArgs = "";
  if (kwargs !== null && kwargs !== undefined && kwargs === Object(kwargs)) {
    _each(kwargs, (v, k) => {

      if (Array.isArray(v)){
          _each(v, (subV) => {
            if (urlArgs !== "") {
              urlArgs += "&";
            }
            urlArgs += `${k}=${subV}`;
          });
      } else {
        if (urlArgs !== "") {
          urlArgs += "&";
        }
        urlArgs += `${k}=${v}`;
      }
    });

    if (urlArgs !== "") {
      urlArgs = `?${urlArgs}`;
    }
  }
  return urlArgs;
};


export default asUrlQuery;
