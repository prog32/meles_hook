export const dateSerializer = v => {
  return new Date(v).toISOString();
};

export default dateSerializer;
