import { useState } from "react";
import _map from "lodash/map";
import _filter from "lodash/filter";
import _omit from "lodash/omit";
import _uniq from "lodash/uniq";

const useSelectManager = ({keyField}) => {
  const [selectedKeys, setSelectedKeys] = useState([]);
  const [selectedObjs, setSelectedObjs] = useState({});

  return {
    count: selectedKeys.length,

    items: selectedObjs,
    "getKeys": () => {
      return selectedKeys;
    },
    onChange: (item, isSelected) => {
      const key = item[keyField || "id"];
      if (isSelected) {
        setSelectedKeys(
          _uniq([...selectedKeys, key])
        );
        setSelectedObjs({
          ...selectedObjs,
          [key]: item,
        });
      } else {
        setSelectedKeys(_filter(selectedKeys, skey => skey !== key));
        setSelectedObjs(_omit(selectedObjs, key));
      }
    },

    onChangeAll: (isSelected, items) => {

      const itemKeys = _map(
        items,
        d => { return d[keyField || "id"]; }
      );

      if (isSelected) {
        // select!
        setSelectedKeys(_uniq([...selectedKeys, ...itemKeys]));

        // generate new obj dict
        const newItems = {
          ...selectedObjs
        };
        for (var i = 0; i < items.length; i++) {
          const item = items[i];
          const key = item[keyField || "id"];
          newItems[key] = item;
        }
        setSelectedObjs(newItems);
      } else {
        // unselect
        setSelectedKeys(_filter(
          selectedKeys,
          skey => {
            // filter - keep if not found in items
            return itemKeys.indexOf(skey) === -1;
          }
        ));
        setSelectedObjs(_omit(
          selectedObjs,
          itemKeys
        ));
      }
    },
    clear: () => {
      setSelectedKeys([]);
      setSelectedObjs({});
    }
  };
};

export default useSelectManager;
