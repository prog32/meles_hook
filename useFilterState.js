import { useState, useEffect } from "react";

import { dateSerializer } from './utils/dateSerializer';

const filterStoreLoadState = (filterStore, newState) => {
  if (filterStore === undefined){
    return;
  }


  try {
    const data = window.localStorage.getItem(filterStore.key);

    if (data === undefined || data === "undefined"){
      return;
    }


    const parsedData = JSON.parse(data);


    if (data === undefined){
      return;
    }

    for (const key in parsedData) {
      if (filterStore.filterKeys)
      if (filterStore.filterKeys !== undefined){
        // only pickup specyfic keys
        if (filterStore.filterKeys.indexOf(key) === -1){
            continue
        }
      }
      if (parsedData.hasOwnProperty(key) ) {
        newState[key] = parsedData[key];
      }
    }

  } catch (error) {
    console.log(error);
  }

}


const filterStoreSaveState = (filterStore, newState) => {
  if (filterStore.key === undefined){
      return;
  }

  try {

      let serializedData;

        if (filterStore.filterKeys === undefined){
          serializedData = JSON.stringify(newState);
        } else {
          serializedData = JSON.stringify(newState, filterStore.filterKeys);
        }

      window.localStorage.setItem(filterStore.key, serializedData);
    } catch (error) {
      console.log(error);
    }
}

const filterInitial = (initialOpts, clean, filterStore) => {
  const opts = initialOpts || {};
  const newState = {};

  for (const key in opts) {
    if (opts.hasOwnProperty(key)) {
      if (opts[key] !== undefined || (Array.isArray(opts[key]) && opts[key].length) ){
        newState[key] = opts[key];
      }
    }}

    filterStoreLoadState(filterStore, newState);

    if (clean !== undefined){
      clean(newState);
    }
  return newState;

};

const mergeWithDelayState = (state, delayedState) => {
  if (delayedState === null){
    return {
      ...state
    };
  }

  const newState = {
    ...state,
    ...delayedState
  };
  for (const key in newState) {
    if (!newState.hasOwnProperty(key)) {
      continue;
    }

    if (newState[key] === undefined){
      delete newState[key];
    }
  }

  return newState;
};

function skipPageKey(key, value)
{
    if (key=="page") return undefined;
    return value;
}

export const useFilterState = (initialStatus, options) => {
  const localOptions = (options || {});
  const delay = localOptions.delay || 500; // delay in ms
  const localStorageKey = localOptions.localStorageKey || undefined;

  const [state, setNewState] = useState(
    () => filterInitial(
        initialStatus,
        (options || {}).clean,
        localOptions.store || {}
      )
  );

  const [maxPage, setMaxPage] = useState(1);

  const [filtersKey, setFiltersKey] = useState(1);

  const setState = (newState) => {
    setNewState(newState)
    filterStoreSaveState(
      localOptions.store || {}, newState
    );
  }
  // -------- delay: debounce ---------
  const [delayedState, setDelayedState] = useState(null);
  const isDelayed = delayedState !== null;

  useEffect(
    () => {
      // Update debounced value after delay

      let timerId = null;
      if (isDelayed){
        timerId = setTimeout(() => {
          if (!isDelayed){
            return;
          }

          const newState = mergeWithDelayState(state, delayedState);

          setDelayedState(null);
          setState(newState);

        }, delay);
      }

      return () => {
        if (timerId) {
          clearTimeout(timerId);
          timerId = null;
        }
      };
    },
    [isDelayed, delay]
  );

  // ------------------------


  const setFilter = (key, val, mode=undefined) => {
    let newState = mergeWithDelayState(state, delayedState);

    let newVal = val;

    if (mode === 'set_add' || mode === 'set_remove'){

      newVal = newState[val];
      if (newVal === undefined){
        newVal = "";
      }

      newVal = newVal.split(",");

      let foundIdx = newVal.indexOf(val);
      if (foundIdx > -1){
        // exists

        if (mode == 'set_remove'){
          array.splice(foundIdx, 1);
        }
        // for set_add - it's ok, no need to add

      } else {
        // missing

        if (mode == 'set_add'){
          newVal.push(val);
        }
        // for set_remove - it's ok, no need to remove
      }

      newVal = join(",");
    }

    newState = {
      ...newState,
      [key]: newVal
    };
    if (options && options.clean !== undefined){
      options.clean(newState);
    }
    if (newState[key] === undefined || newState[key] === []){
      delete newState[key];
    }

    const newFiltersKey = JSON.stringify(newState, skipPageKey);
    if (newFiltersKey != filtersKey){
      setFiltersKey(newFiltersKey);
      if (key !== "page"){
        newState['page'] = 1;
      }
    }

    if (newState.page !== undefined && newState.maxPage !== undefined){
        if (newState.page > newState.maxPage){
          newState['page'] = 1
        }
    }

    if (isDelayed) {
      setDelayedState(null);
    }
    setState(newState);
  };

  return {
    filters: state,
    getKey: () => {
      return JSON.stringify(state);
    },
    setMaxPage: maxPage => {
      setMaxPage(maxPage);

      if (state.page !== undefined && state.page > maxPage){
        const newState = {
          ...state,
          page: maxPage
        };
        setState(newState);
      }

    },
    setPage: pageId => setFilter("page", pageId),
    set: setFilter,

    setToggle: (key, defaultValue) => {
      let cleanVal = state[key];
      if (cleanVal === undefined){
          cleanVal = defaultValue;
      }
      setFilter(key, !cleanVal);
    },

    setAdd: (key, value) => {
      setFilter(key, value, 'set_add');
    },
    setRemove: (key) => {
      setFilter(key, value, 'set_remove');
    },
    setDateTime: (k, v) => {
      setFilter(k, dateSerializer(v));
    },
    setDebounced: (key, val) => {
      setDelayedState({ ...delayedState, [key]: val });

    },
    getSetter: key => {
      return val => setFilter(key, val);
    },
    getCheckState: key => {
      return state[key] === undefined ? null : state[key];
    },
    handleTableChange: (
      type,
      { page, sizePerPage, filters, sortField, sortOrder, cellEdit }
    ) => {
      const newState = { ...state };
      if (sizePerPage) {
        newState.page_size = sizePerPage;
      } else if (state.sizePerPage) {
        newState.page_size = state.sizePerPage;
      }
      if (sortField) {
        newState.order_by = `${
          sortOrder === "asc" ? "" : "-"
        }${sortField}`;
      }
    
      setState(newState);
    }
  };
};

export default useFilterState;
