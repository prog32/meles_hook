import { useEffect } from "react";

import useFetchState from "./useFetchState";


// ====================
// Abort support
// ====================
// Note that the "unfetch" minimal fetch polyfill defines fetch() without
// defining window.Request, and this polyfill need to work on top of unfetch
// so the below feature detection needs the !self.AbortController part.
// The Request.prototype check is also needed because Safari versions 11.1.2
// up to and including 12.1.x has a window.AbortController present but still
// does NOT correctly implement abortable fetch:
// https://bugs.webkit.org/show_bug.cgi?id=174980#c2
const shouldPatchAbortController =
  (typeof window.Request === "function" &&
    !window.Request.prototype.hasOwnProperty("signal")) ||
  !window.AbortController;


// ====================
// Example
// ====================
// const dataFS = useFetch(fetcher, conditions);
// dataFS = {
//     'isFetching': false,
//     'isReady': false,
//     'isSuccess': null,
//     'isFailure': null,
//     'error': null,
//     'data': initialData === undefined ? null : initialData,
//     'fetch': () => ..
// }


export const useFetch = (fetcher, conditions, options) => {
  const optionsWithDefaults = Object.assign(
    {},
    {
      initialData: null,
      persistent: true,
    },
    options
  );

  const [data, setState] = useFetchState(
    optionsWithDefaults.initialData,
    optionsWithDefaults.persistent,
    options
  );

  const fetch = (opts) => {
    const { onSuccess, onFailure, ...options } = opts || {};



    if (fetcher !== null && fetcher !== undefined){
      setState.fetching();

      fetcher(options)
        .then((...args) => {
          setState.success(...args);
        })
        .catch((...args) => {
          setState.failure(...args);
        });



      } else {
       setState.success(null);
      }



  }
  const fetchCancelable = () => {
    let controller = null;
    let signal = null;

    if (!shouldPatchAbortController) {
      controller = new AbortController();
      signal = controller.signal;
    }

    if (signal) {
      fetch({
        signal: signal,
      });
    } else {
      fetch();
    }

    return () => {
      if (controller) {
        controller.abort();
      }
    };
  };

  useEffect(
    () => {

      let shouldFetch = true;
      if (optionsWithDefaults.shouldFetch !== undefined) {
        shouldFetch = optionsWithDefaults.shouldFetch();
      }

      if (shouldFetch) {
        return fetchCancelable();
      }

      return () => {};
    },
    conditions === undefined ? [] : conditions
  );

  return {
    ...data,

    // Extra methods
    fetch,
    fetchCancelable,
    setData: (...args) => {
      setState.success(...args);
    },
  };
};

export default useFetch;
