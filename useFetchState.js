import { useState } from "react";

// Example
export const useFetchCompleteState = (json) => {
  return {
    isReady: true,
    isFetching: false,
    isSuccess: true,
    isFailure: false,
    data: json,
    dataTimeStamp: Date.now(),
    responseTimeStamp: Date.now(),
    error: null,
  };
};
export const useFetchState = (initialData, persistent = false, options) => {
  // pagination: {count: 54, per_page: 500, page_count: 1, page: 1}

  const [data, setData] = useState(() => {
    let initData = initialData === undefined ? null : initialData;
    if (initData instanceof Function) {
      initData = initData();
    }
    return {
      isReady: false,
      isFetching: false,
      isSuccess: null,
      isFailure: null,
      data: initData,
      error: null,
      dataTimeStamp: null,
      responseTimeStamp: null,
      hasData: initData !== null,
      hasError: false,
    };
  });

  const success = (json) => {
    const newData = {
      isReady: true,
      isFetching: false,
      isSuccess: true,
      isFailure: false,
      data: json,
      dataTimeStamp: Date.now(),
      responseTimeStamp: Date.now(),
      error: null,
    };
    setData(newData);
    if (options && options.onSuccess) {
      options.onSuccess(data);
    }
  };
  const failure = (errorJSON) => {
    let json = errorJSON || {};
    let error = {
      ...json,
    };
    if (json.type === "server") {
      error = {
        http_code: json.status,
      };
    }
    const newData = {
      isReady: true,
      isFetching: false,
      isSuccess: false,
      isFailure: true,
      data: null,
      dataTimeStamp: null,
      responseTimeStamp: Date.now(),
      error,
    };
    if (persistent) {
      if (error && error.type === "network") {
        newData.dataTimeStamp = data.dataTimeStamp;
        newData.data = data.data;
      }
    }
    setData(newData);
  };
  const fetching = () => {
    const newData = {
      isReady: false,
      isFetching: true,
      isSuccess: null,
      isFailure: null,
      data: null,
      dataTimeStamp: null,
      responseTimeStamp: data.responseTimeStamp,
      error: null,
    };
    if (persistent) {
      newData.dataTimeStamp = data.dataTimeStamp;
      newData.data = data.data;
    }
    setData(newData);
  };

  data.hasData = data.data !== null;
  data.hasError = data.error !== null;

  return [
    data,
    {
      success,
      failure,
      fetching,
    },
  ];
};

export default useFetchState;
