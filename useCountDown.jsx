import differenceInSeconds from 'date-fns/differenceInSeconds';

import {useState, useEffect} from 'react';

const trimNegative = val => {
  return val === null || val > 0 ? val : 0;
};

export const useCountDown = date => {
  const [secondsLeft, setCounter] = useState(null);
  useEffect(() => {
    if (secondsLeft === null || secondsLeft > 0) {
      const timerId = setTimeout(() => {
        if (!date) {
          return null;
        }
        setCounter(differenceInSeconds(date, new Date()));
      }, 1000);

      return () => {
        clearTimeout(timerId);
      };
    }
  }, [secondsLeft, date + '']);

  if (secondsLeft === null) {
    if (date) {
      return trimNegative(differenceInSeconds(date, new Date()));
    }
    return null;
  }
  return trimNegative(secondsLeft);
};

export default useCountDown;
