import httpReq from "./utils/httpReq";
import asUrlQuery from "./utils/asUrlQuery";
import useFetch from "./useFetch";
import useFilterState from "./useFilterState";
import useSelection from "./useSelection";

export const useFetchList = (url, filtersInit, options) => {
  const fm = useFilterState(filtersInit, options);
  const fetchState = useFetch((fetchOptions) => {
    const urlWithParams = url + asUrlQuery(fm.filters);
    return httpReq.get(urlWithParams, fetchOptions);
  }, [fm.getKey()]);

  let selection = null;
  if (options && options.selection !== undefined){
      selection = useSelection(options.selection);
  }

  return {
    filter: fm,
    selection: selection,
    ...fetchState,
  };
};

export default useFetchList;
